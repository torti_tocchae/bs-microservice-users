This is the assignment project from UXP. 
This is a basic tasks management microservice built with sring-rest and spring-jpa
with an in memory HSQLDB Database. On reboot of the application, the database will be
cleaned out.

In order to build and run this project you must ensure you have the following installed and setup:

1. Maven - at least version 3.3
2. Java - at least version 1.8 (Has not been tested with 1.9)

To compile project please run the following from the root directory of the project:
    mvn clean install

The project is built with spring boot, and has an embedded tomcat server, so in order to run the project,
please execute the following command:
    mvn spring-boot:run

The project can be accessed from this URL:

    http://localhost:8080/api --> This will load the swagger UI and allow you a view of all the endpoints

The scheduled job to change the status of expired Taska from Pennding to done has been configured to run every minute. Feel free to modify this setting in the application.properties file.

Some Known Issues:

When swagger UI is loaded, it throws an error, because it tries to fill a default string value for one of the endpoint numeric fields. Please ignore this error.
It will not impact running and executing the endpoints. This will be fixed in the next version.

ENJOY!!! 
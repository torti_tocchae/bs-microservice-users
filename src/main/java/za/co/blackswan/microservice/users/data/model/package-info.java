/**
 * This package contains database entity classes and their meta models.
 * 
 * @since 1.0
 * @author Torti Ama-Njoku @ UXP
 * @version 1.0
 */
package za.co.blackswan.microservice.users.data.model;

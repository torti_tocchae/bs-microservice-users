package za.co.blackswan.microservice.users.type;

import za.co.blackswan.microservice.users.data.model.User;

/**
 * Enum class to hold {@link User} statuses
 * 
 * @author Torti Ama-Njoku @ Black Swan
 */
public enum UserStatusEnum {
    
    /**
     * Active {@link User} status
     */
    ACTIVATED("Activated"),
    /**
     * An inactive {@link User} status
     */
    DEACTIVATED("Deactivated");

    private final String labelKey;
    
    /**
     * Initialisation
     *
     * @param labelKey replacement string
     */
    UserStatusEnum(String labelKey) {
            this.labelKey = labelKey;
    }
    
    /**
     * {@link UserStatusEnum} String representation
     *
     * @return String
     */
    public String getKeyValue() {
            return labelKey;
    }
}

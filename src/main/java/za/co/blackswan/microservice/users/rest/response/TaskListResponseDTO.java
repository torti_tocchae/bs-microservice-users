package za.co.blackswan.microservice.users.rest.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import java.util.List;
import za.co.blackswan.microservice.users.data.model.Task;

/**
 * REST webservice response entity for list of {@link Task}s
 *
 * @author Torti Ama-Njoku @ Black Swan
 */
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class TaskListResponseDTO extends GenericWSResponseDTO {

    private List<Task> tasks;

    /**
     * Default Constructor
     */
    public TaskListResponseDTO() {
        super();
    }

    /**
     * @param responseCode webservice response code
     * @param responseMessage webservice response message
     * @param status HTTP status code
     * @param timeStamp time stamp of the reponse
     */
    public TaskListResponseDTO(String responseCode, String responseMessage,
                               int status, long timeStamp) {
        super(responseCode, responseMessage, status, timeStamp);
    }

    /**
     * @param responseCode webservice response code
     * @param responseMessage webservice response message
     * @param status HTTP status code
     * @param timeStamp time stamp of the reponse
     * @param tasks list of tasks to be returned
     */
    public TaskListResponseDTO(String responseCode, String responseMessage,
                               int status, long timeStamp, List<Task> tasks) {
        super(responseCode, responseMessage, status, timeStamp);
        this.tasks = tasks;
    }

    /**
     * Gets tasks.
     *
     * @return Value of tasks.
     */
    public List<Task> getTasks() {
        return tasks;
    }

    /**
     * Sets new tasks.
     *
     * @param tasks New value of tasks.
     */
    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }
}

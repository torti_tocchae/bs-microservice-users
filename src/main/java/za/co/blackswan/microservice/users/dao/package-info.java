/**
 * This package contains data access objects, based on JPA repository factory functions
 * on how to save, delete, find data etc.
 * 
 * @since 1.0
 * @author Torti Ama-Njoku @ Black Swan
 * @version 1.0
 */
package za.co.blackswan.microservice.users.dao;

package za.co.blackswan.microservice.users.rest.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import java.util.List;
import za.co.blackswan.microservice.users.data.model.User;

/**
 * REST webservice response entity for list of {@link User}s
 *
 * @author Torti Ama-Njoku @ Black Swan
 */
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class UserListResponseDTO extends GenericWSResponseDTO {

    private List<User> users;
    
    /**
     * Default Constructor
     */
    public UserListResponseDTO() {
        super();
    }
    
    /**
     * @param responseCode webservice response code
     * @param responseMessage webservice response message
     * @param status HTTP status code
     * @param timeStamp time stamp of the reponse
     */
    public UserListResponseDTO(String responseCode, String responseMessage,
                               int status, long timeStamp) {
        super(responseCode, responseMessage, status, timeStamp);
    }

    /**
     * @param responseCode webservice response code
     * @param responseMessage webservice response message
     * @param status HTTP status code
     * @param timeStamp time stamp of the reponse
     * @param users list of users to be returned
     */
    public UserListResponseDTO(String responseCode, String responseMessage,
                               int status, long timeStamp, List<User> users) {
        super(responseCode, responseMessage, status, timeStamp);
        this.users = users;
    }

    /**
     * Gets users.
     *
     * @return Value of users.
     */
    public List<User> getUsers() {
        return users;
    }

    /**
     * Sets new users.
     *
     * @param users New value of users.
     */
    public void setUsers(List<User> users) {
        this.users = users;
    }
}

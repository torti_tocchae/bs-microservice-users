package za.co.blackswan.microservice.users.service;

import java.util.Date;
import java.util.List;
import za.co.blackswan.microservice.users.data.model.Task;
import za.co.blackswan.microservice.users.data.model.User;
import za.co.blackswan.microservice.users.exception.EntityNotFoundException;
import za.co.blackswan.microservice.users.exception.InvalidParameterException;
import za.co.blackswan.microservice.users.exception.MandatoryDetailsException;

/**
 * Interface service class for executing desired {@link Task} object operations
 * @author Torti Ama-Njoku @ Black Swan
 */
public interface TaskService {
    
    /**
     * Get a list of all {@link User}s {@link Task}s
     * @param userId {@link User} t retrieve tasks for
     * @return retrieved list of {@link User} objects
     * @throws EntityNotFoundException if the {@link User} is not found
     */
    List<Task> getAllUserTasks(Long userId) throws EntityNotFoundException;
    
    /**
     * Find a {@link Task} by the {@link Task} ID, i.e. the primary identifier in the table
     * @param id primary identifier value of the {@link Task} to be found
     * @return retrieved {@link Task} object
     * @throws EntityNotFoundException if the {@link Task} is not found
     */
    Task findById(Long id) throws EntityNotFoundException;

    /**
     * Find a {@link Task} by the {@link Task} ID and checks if it belongs to the given {@link User}
     * @param id primary identifier value of the {@link Task} to be found
     * @param userId {@link User} the task belongs to
     * @return retrieved {@link Task} object
     * @throws EntityNotFoundException if the {@link Task} is not found
     * @throws InvalidParameterException if the given {@link Task} ID does not belong to the given {@link User} ID
     */
    Task findById(Long id, Long userId) throws EntityNotFoundException, InvalidParameterException;

    /**
     * Gets a list of all pending and expired {@link Task}s
     * @return a list of all pending and expired {@link Task}s
     */
    List<Task> getAllPendingExpiredTasks();
    
    /**
     * Delete a {@link Task} from the database
     * @param id primary identifier value of the {@link Task} to be deleted
     * @param userId {@link User} the task belongs to
     * @throws EntityNotFoundException if the {@link Task} is not found
     * @throws InvalidParameterException if the given {@link Task} ID does not belong to the given {@link User} ID
     */
    void deleteTask(Long id, Long userId) throws EntityNotFoundException, InvalidParameterException;
    
    /**
     * Update a {@link Task}'s details
     * @param taskId {@link Task} to be updated
     * @param userId {@link User} the task belongs to
     * @param name new {@link Task} name
     * @param description new {@link Task} description
     * @param dateTime new {@link Task} scheduled time
     * @return updated {@link Task} object
     * @throws EntityNotFoundException if the {@link Task} to be updated is not found
     * @throws InvalidParameterException if the given {@link Task} ID does not belong to the given {@link User} ID
     */
    Task updateTask(Long taskId, Long userId, String name, String description, Date dateTime)
            throws EntityNotFoundException, InvalidParameterException;
    
    /**
     * Create a new {@link Task}
     * @param userId {@link User} the task belongs to
     * @param name {@link Task}'s name
     * @param description {@link Task}'s description/comments
     * @param dateTime date and time the {@link Task} is scheduled for
     * @return the created {@link Task} entity
     * @throws EntityNotFoundException if the {@link User} with the given ID is not found
     * @throws MandatoryDetailsException if there is some missing mandatory details
     */
    Task createTask(Long userId, String name, String description, Date dateTime)
            throws EntityNotFoundException, MandatoryDetailsException;

    /**
     * Save multiple {@link Task}s in a batch
     * @param tasksToBeSaved collection all the {@link Task}s to be saved
     * @return collection of the saved tasks
     */
    List<Task> saveAll(Iterable<Task> tasksToBeSaved);
}
/**
 * This package contains all enums used in the application.
 * 
 * @since 1.0
 * @author Torti Ama-Njoku @ Black Swan
 * @version 1.0
 */
package za.co.blackswan.microservice.users.type;

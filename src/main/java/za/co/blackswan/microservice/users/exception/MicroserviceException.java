package za.co.blackswan.microservice.users.exception;

import za.co.blackswan.microservice.users.type.ResponseEnum;

/**
 * Exception super class.
 *
 * @author Torti Ama-Njoku @ Black Swan
 */
@SuppressWarnings("serial")
public class MicroserviceException extends Exception {
    
    private ResponseEnum exceptionResponse;
    
    /**
     * Default Constructor
     */
    public MicroserviceException() { }

    /**
     * Constructor to with exception message
     * @param message exception message
     * @param exceptionResponse the response object associated with this exception
     */
    public MicroserviceException(String message, ResponseEnum exceptionResponse) {
        super(message);
        this.exceptionResponse = exceptionResponse;
    }

    /**
     * Constructor with message and exception cause
     * @param message exception message
     * @param cause exception cause
     * @param exceptionResponse the response object associated with this exception
     */
    public MicroserviceException(String message, Throwable cause, ResponseEnum exceptionResponse) {
        super(message, cause);
        this.exceptionResponse = exceptionResponse;
    }

    /**
     * @return the exceptionResponse
     */
    public ResponseEnum getExceptionResponse() {
        return exceptionResponse;
    }

    /**
     * @param exceptionResponse the exceptionResponse to set
     */
    public void setExceptionResponse(ResponseEnum exceptionResponse) {
        this.exceptionResponse = exceptionResponse;
    }
}
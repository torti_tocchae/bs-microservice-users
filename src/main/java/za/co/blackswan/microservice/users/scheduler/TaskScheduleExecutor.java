package za.co.blackswan.microservice.users.scheduler;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import za.co.blackswan.microservice.users.data.model.Task;
import za.co.blackswan.microservice.users.service.TaskService;
import za.co.blackswan.microservice.users.type.TaskStatusEnum;

/**
 * Executes all task related scheduled processes
 * @author Torti Ama-Njoku @ Black Swan
 */
@Component
public class TaskScheduleExecutor {

    private static final Logger LOGGER = LoggerFactory.getLogger(TaskScheduleExecutor.class);
    private TaskService taskService;

    /**
     * Constructor with applicable parameters
     * @param taskService {@link TaskService} for operating on the {@link Task}s
     */
    public TaskScheduleExecutor(final TaskService taskService) {
        this.taskService = taskService;
    }

    /**
     * Method that is run by the cron to process all expired and pending {@link Task}s and change their statuses to done.
     */
    //@Scheduled(cron = "${bs.pending.task.cron}")
    @Scheduled(cron = "${bs.pending.task.cron}")
    @Transactional
    public void processAllPendingTasks() {
        List<Task> tasks = taskService.getAllPendingExpiredTasks();

        // Convert each task to status done
        if (tasks != null && !tasks.isEmpty()) {
            tasks.forEach(task -> {
                // Change the status of the task to done
                LOGGER.info("Task with id {} has expired and is being set to status DONE", task.getId());
                task.setStatus(TaskStatusEnum.DONE);
            });

            taskService.saveAll(tasks);
        }
    }
}

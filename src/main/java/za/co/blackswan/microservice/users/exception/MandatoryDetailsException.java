package za.co.blackswan.microservice.users.exception;

import za.co.blackswan.microservice.users.type.ResponseEnum;

/**
 * Exception class to define exception for when mandatory details is missing 
 * from user entity usually thrown on creation or update.
 *
 * @author Torti Ama-Njoku @ Black Swan
 */
@SuppressWarnings("serial")
public class MandatoryDetailsException extends MicroserviceException {

    /**
     * Default Constructor
     */
    public MandatoryDetailsException() { }

    /**
     * Constructor to with exception message
     * @param message exception message
     * @param exceptionResponse the response object associated with this exception
     */
    public MandatoryDetailsException(String message, ResponseEnum exceptionResponse) {
        super(message, exceptionResponse);
    }

    /**
     * Constructor with message and exception cause
     * @param message exception message
     * @param cause exception cause
     * @param exceptionResponse the response object associated with this exception
     */
    public MandatoryDetailsException(String message, Throwable cause, ResponseEnum exceptionResponse) {
        super(message, cause, exceptionResponse);
    }
}

package za.co.blackswan.microservice.users.exception;

import za.co.blackswan.microservice.users.type.ResponseEnum;

/**
 * Exception class to define exception for when a parameter does not pass validation.
 *
 * @author Torti Ama-Njoku @ Black Swan
 */
@SuppressWarnings("serial")
public class InvalidParameterException extends MicroserviceException {

    /**
     * Default Constructor
     */
    public InvalidParameterException() { }

    /**
     * Constructor to with exception message
     * @param message exception message
     * @param exceptionResponse the response object associated with this exception
     */
    public InvalidParameterException(String message, ResponseEnum exceptionResponse) {
        super(message, exceptionResponse);
    }

    /**
     * Constructor with message and exception cause
     * @param message exception message
     * @param cause exception cause
     * @param exceptionResponse the response object associated with this exception
     */
    public InvalidParameterException(String message, Throwable cause, ResponseEnum exceptionResponse) {
        super(message, cause, exceptionResponse);
    }
}

package za.co.blackswan.microservice.users.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.co.blackswan.microservice.users.dao.TaskDao;
import za.co.blackswan.microservice.users.data.model.Task;
import za.co.blackswan.microservice.users.data.model.User;
import za.co.blackswan.microservice.users.exception.EntityNotFoundException;
import za.co.blackswan.microservice.users.exception.InvalidParameterException;
import za.co.blackswan.microservice.users.exception.MandatoryDetailsException;
import za.co.blackswan.microservice.users.service.TaskService;
import za.co.blackswan.microservice.users.service.UserService;
import za.co.blackswan.microservice.users.type.ResponseEnum;

/**
 * Implementation of {@link TaskService} interface.
 * 
 * @author Torti Ama-Njoku @ Black Swan
 */
@Service("taskService")
public class TaskServiceImpl implements TaskService {

    private static final Locale LOCALE = new Locale.Builder().setLanguage("en").setRegion("ZA").build();

    private final TaskDao taskDao;

    private final UserService userService;

    private final MessageSource messageSource;

    /**
     * Used by spring to instantiate a bean of this class autowiring by argument
     * resolvers the parameters in this constructor.
     *
     * @param taskDao {@link Task} data access object
     * @param userService {@link User} service interface for operations
     * @param messageSource source for error messages
     */
    public TaskServiceImpl(final TaskDao taskDao,
            final UserService userService, final MessageSource messageSource) {
        this.taskDao = taskDao;
        this.userService = userService;
        this.messageSource = messageSource;
    }
    
    @Transactional(readOnly = true)
    @Override
    public List<Task> getAllUserTasks(Long userId) throws EntityNotFoundException {
        // Check if the user exists
        userService.findById(userId);
        List<Task> found = taskDao.getAllByUser(userId);
        return found;
    }
    
    @Transactional(readOnly = true)
    @Override
    public Task findById(Long id) throws EntityNotFoundException {
        Optional<Task> found = taskDao.findById(id);
        if (!found.isPresent()) {
            throw new EntityNotFoundException(
                //user.notFound.id
                messageSource.getMessage(ResponseEnum.RESPONSE_CODE_210.getLabelKey(),
                        new Object[] {id}, LOCALE),
                ResponseEnum.RESPONSE_CODE_210);
        }
        return found.get();
    }

    @Transactional(readOnly = true)
    @Override
    public Task findById(Long id, Long userId) throws EntityNotFoundException, InvalidParameterException {
        Optional<Task> found = taskDao.findById(id);
        if (!found.isPresent()) {
            throw new EntityNotFoundException(
                    //user.notFound.id
                    messageSource.getMessage(ResponseEnum.RESPONSE_CODE_210.getLabelKey(),
                            new Object[] {id}, LOCALE),
                    ResponseEnum.RESPONSE_CODE_210);
        }

        verifyTaskUserCorrelation(found.get(), userId);

        return found.get();
    }

    @Transactional(readOnly = true)
    @Override
    public List<Task> getAllPendingExpiredTasks() {
        return taskDao.getAllPendingExpiredTasks(new Date());
    }
    
    @Transactional
    @Override
    public void deleteTask(Long id, Long userId) throws EntityNotFoundException, InvalidParameterException {
        Task toBeDeleted = findById(id);

        verifyTaskUserCorrelation(toBeDeleted, userId);

        taskDao.delete(toBeDeleted);
    }
    
    @Transactional
    @Override
    public Task updateTask(Long taskId, Long userId, String name, String description, Date dateTime)
            throws EntityNotFoundException, InvalidParameterException {

        // Get the task to be updated
        Task taskToUpdate = findById(taskId);

        verifyTaskUserCorrelation(taskToUpdate, userId);

        // Update the task with the new details
        if (StringUtils.isNotBlank(name)) {
            taskToUpdate.setName(name);
        }

        if (StringUtils.isNotBlank(description)) {
            taskToUpdate.setDescription(description);
        }

        if (Objects.nonNull(dateTime)) {
            taskToUpdate.setDateTime(dateTime);
        }
        
        // Save the new and return the new task
        return taskDao.saveAndFlush(taskToUpdate);
    }

    @Transactional
    @Override
    public Task createTask(Long userId, String name, String description, Date dateTime)
            throws EntityNotFoundException, MandatoryDetailsException {

        // Find the user
        User user = userService.findById(userId);

        // Setup the task object to be saved
        Task task = new Task(user, name, description, dateTime);

        // Check if there is any missing mandatory details, throw the error
        if (task.hasMissingDetails()) {
            throw new MandatoryDetailsException(
                    //user.save.mandatoryDetails
                    messageSource.getMessage(ResponseEnum.RESPONSE_CODE_211.getLabelKey(),
                            null, LOCALE),
                    ResponseEnum.RESPONSE_CODE_211);
        }

        // Save the new task and return the new entity
        return taskDao.saveAndFlush(task);
    }

    @Override
    public List<Task> saveAll(Iterable<Task> tasksToBeSaved) {
        return taskDao.saveAll(tasksToBeSaved);
    }

    /**
     * Verifies if the given {@link Task} belongs to the given {@link User}
     * @param task {@link Task} to be verified
     * @param userId primary identifier of the {@link User} to whom the {@link Task} should belong to
     * @throws InvalidParameterException if the given {@link Task} does not belong to the given {@link User}
     */
    private void verifyTaskUserCorrelation(Task task, Long userId) throws InvalidParameterException {
        if (!task.getUser().getId().equals(userId)) {
            throw new InvalidParameterException(
                    //task.user.mismatch
                    messageSource.getMessage(ResponseEnum.RESPONSE_CODE_212.getLabelKey(),
                            new Object[] {userId, task.getId()}, LOCALE),
                    ResponseEnum.RESPONSE_CODE_212);
        }
    }

}
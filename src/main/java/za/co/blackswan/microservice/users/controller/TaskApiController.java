package za.co.blackswan.microservice.users.controller;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import za.co.blackswan.microservice.users.data.model.Task;
import za.co.blackswan.microservice.users.data.model.User;
import za.co.blackswan.microservice.users.exception.EntityNotFoundException;
import za.co.blackswan.microservice.users.exception.InvalidParameterException;
import za.co.blackswan.microservice.users.exception.MandatoryDetailsException;
import za.co.blackswan.microservice.users.rest.request.CreateTaskRequestDTO;
import za.co.blackswan.microservice.users.rest.request.UpdateTaskRequestDTO;
import za.co.blackswan.microservice.users.rest.request.UpdateUserRequestDTO;
import za.co.blackswan.microservice.users.rest.response.TaskListResponseDTO;
import za.co.blackswan.microservice.users.rest.response.TaskResponseDTO;
import za.co.blackswan.microservice.users.service.TaskService;
import za.co.blackswan.microservice.users.type.ResponseEnum;

/**
 * API Class that holds all the webservices for {@link Task} operations
 * @author Torti Ama-Njoku @ Black Swan
 */
@RestController
@RequestMapping(value = "/user/{user_id}/task")
@Transactional
@Validated
public class TaskApiController {

    // Define the logger object for this class
    private static final Logger LOGGER = LoggerFactory.getLogger(TaskApiController.class);
    private static final Locale LOCALE = new Locale.Builder().setLanguage("en").setRegion("ZA").build();

    private final TaskService taskService;
    private final MessageSource messageSource;

    /**
     * Spring will autowire the parameters of this method by argument
     * resolvers the parameters in this constructor.
     *
     * @param taskService {@link Task} service singleton used to operate on the {@link Task} data
     * @param messageSource source for configured messages
     */
    public TaskApiController(final TaskService taskService, final MessageSource messageSource) {
        this.taskService = taskService;
        this.messageSource = messageSource;
    }

    /**
     * Webservice API called when a {@link Task} is created
     * @param userId primary identifier of the {@link User} for which the task is to be created
     * @param dataHolderRequest {@link CreateTaskRequestDTO} webservice rest input object
     * @return {@link ResponseEntity} HTTP response entity with JSON response attributes
     * @throws MandatoryDetailsException if some mandatory details are missing
     * @throws EntityNotFoundException if the user to create the {@link Task} for does not exist
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<TaskResponseDTO> createTask(
            @PathVariable(name = "user_id") Long userId,
            @Valid @RequestBody final CreateTaskRequestDTO dataHolderRequest)
            throws MandatoryDetailsException, EntityNotFoundException {

        LOGGER.debug("Calling createTask");

        // Calling the user creation method
        Task createdTask = taskService.createTask(userId, dataHolderRequest.getName(), dataHolderRequest.getDescription(),
                dataHolderRequest.getDateTime());

        // Build response object
        TaskResponseDTO response = new TaskResponseDTO(ResponseEnum.SUCCESSFUL.getCode(),
                messageSource.getMessage(ResponseEnum.SUCCESSFUL.getLabelKey(),
                        new Object[]{messageSource.getMessage("task.creation.successful", null, LOCALE)},
                        LOCALE),
                ResponseEnum.SUCCESSFUL.getHttpCode().value(), new Date().getTime(), createdTask);

        return new ResponseEntity<>(response, ResponseEnum.SUCCESSFUL.getHttpCode());
    }

    /**
     * Webservice API called when a {@link Task} is updated
     * @param userId id of the {@link User} whose {@link Task} is to be updated
     * @param taskId id of the {@link Task} to be updated
     * @param dataHolderRequest {@link UpdateUserRequestDTO} webservice rest input object
     * @return {@link ResponseEntity} HTTP response entity with JSON response attributes
     * @throws EntityNotFoundException if the {@link Task} to be updated cannot be found
     * @throws InvalidParameterException if the given {@link Task} ID does not belong to the given {@link User} ID
     */
    @RequestMapping(value = "/{task_id}", method = RequestMethod.PUT)
    public ResponseEntity<TaskResponseDTO> updateTask(
            @PathVariable(name = "user_id") Long userId,
            @PathVariable(name = "task_id") Long taskId,
            @Valid @RequestBody UpdateTaskRequestDTO dataHolderRequest)
            throws EntityNotFoundException, InvalidParameterException {

        LOGGER.debug("Calling task update");

        // update task
        Task updatedTask = taskService.updateTask(taskId, userId, dataHolderRequest.getName(),
                                dataHolderRequest.getDescription(), dataHolderRequest.getDateTime());

        // Build response object
        TaskResponseDTO response = new TaskResponseDTO(ResponseEnum.SUCCESSFUL.getCode(),
                messageSource.getMessage(ResponseEnum.SUCCESSFUL.getLabelKey(),
                        new Object[]{messageSource.getMessage("task.updateTask.successful", null, LOCALE)},
                        LOCALE),
                ResponseEnum.SUCCESSFUL.getHttpCode().value(), new Date().getTime(), updatedTask);

        return new ResponseEntity<>(response, ResponseEnum.SUCCESSFUL.getHttpCode());
    }

    /**
     * Webservice API called when a {@link Task} is deleted
     * @param userId id of the {@link User} whose {@link Task} is to be deleted
     * @param taskId id of the {@link Task} to be deleted
     * @return {@link ResponseEntity} HTTP response entity with JSON response attributes
     * @throws EntityNotFoundException if the {@link Task} to be deleted cannot be found
     * @throws InvalidParameterException if the given {@link Task} ID does not belong to the given {@link User} ID
     */
    @RequestMapping(value = "/{task_id}", method = RequestMethod.DELETE)
    public ResponseEntity<TaskResponseDTO> deleteTask(
            @PathVariable(name = "user_id") Long userId,
            @PathVariable(name = "task_id") Long taskId)
            throws EntityNotFoundException, InvalidParameterException {

        LOGGER.debug("Calling task delete");

        // delete task
        taskService.deleteTask(taskId, userId);

        // Build response object
        TaskResponseDTO response = new TaskResponseDTO(ResponseEnum.SUCCESSFUL.getCode(),
                messageSource.getMessage(ResponseEnum.SUCCESSFUL.getLabelKey(),
                        new Object[]{messageSource.getMessage("task.delete.successful", null, LOCALE)},
                        LOCALE),
                ResponseEnum.SUCCESSFUL.getHttpCode().value(), new Date().getTime());

        return new ResponseEntity<>(response, ResponseEnum.SUCCESSFUL.getHttpCode());
    }

    /**
     * Webservice API called to retrieve a {@link User}'s specific {@link Task} information
     * @param userId id of the {@link User} whose {@link Task} is to be retrieved
     * @param taskId id of the {@link Task} to be retrieved
     * @return {@link ResponseEntity} HTTP response entity with JSON response attributes
     * @throws EntityNotFoundException if the {@link Task} to be retrieved cannot be found
     * @throws InvalidParameterException if the given {@link Task} ID does not belong to the given {@link User} ID
     */
    @RequestMapping(value = "/{task_id}", method = RequestMethod.GET)
    public ResponseEntity<TaskResponseDTO> getTask(
            @PathVariable(name = "user_id") Long userId,
            @PathVariable(name = "task_id") Long taskId)
            throws EntityNotFoundException, InvalidParameterException {

        LOGGER.debug("Calling task getTask");

        // find the task
        Task task = taskService.findById(taskId, userId);

        // Build response object
        TaskResponseDTO response = new TaskResponseDTO(ResponseEnum.SUCCESSFUL.getCode(),
                messageSource.getMessage(ResponseEnum.SUCCESSFUL.getLabelKey(),
                        new Object[]{messageSource.getMessage("task.getTask.successful", null, LOCALE)},
                        LOCALE),
                ResponseEnum.SUCCESSFUL.getHttpCode().value(), new Date().getTime(), task);

        return new ResponseEntity<>(response, ResponseEnum.SUCCESSFUL.getHttpCode());
    }

    /**
     * Webservice API called to get a list of all {@link Task}s for a given {@link User} in the system
     * @param userId id of the {@link User} whose {@link Task}s is to be retrieved
     * @return {@link ResponseEntity} HTTP response entity with JSON response attributes
     * @throws EntityNotFoundException if the {@link User} with the given ID cannot be found
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<TaskListResponseDTO> getTasks(@PathVariable(name = "user_id") Long userId)
            throws EntityNotFoundException {

        LOGGER.debug("Calling getTasks");

        // Calling the sign in method
        List<Task> tasks = taskService.getAllUserTasks(userId);

        // Build response object
        TaskListResponseDTO response = new TaskListResponseDTO(ResponseEnum.SUCCESSFUL.getCode(),
                messageSource.getMessage(ResponseEnum.SUCCESSFUL.getLabelKey(),
                        new Object[]{messageSource.getMessage("task.getUserTasks.successful", null, LOCALE)},
                        LOCALE),
                ResponseEnum.SUCCESSFUL.getHttpCode().value(), new Date().getTime(), tasks);

        return new ResponseEntity<>(response, ResponseEnum.SUCCESSFUL.getHttpCode());
    }
}

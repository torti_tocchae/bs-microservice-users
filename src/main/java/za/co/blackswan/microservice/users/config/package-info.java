/**
 * This package contains all spring boot configuration files.
 * 
 * @since 1.0
 * @author Torti Ama-Njoku @ Black Swan
 * @version 1.0
 */
package za.co.blackswan.microservice.users.config;

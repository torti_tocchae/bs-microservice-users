package za.co.blackswan.microservice.users.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Application configuration class
 * @author Torti Ama-Njoku @ Black Swan
 */
@EnableJpaRepositories(basePackages = {"za.co.blackswan.microservice.users.dao"})
@EntityScan(basePackages = {"za.co.blackswan.microservice.users.data.model"})
@EnableSwagger2
@ComponentScan(basePackages = {"za.co.blackswan.microservice.users"})
@Configuration
public class UsersMicroserviceConfig {

    /**
     * Password encoder bean for password encryption
     * @return {@link PasswordEncoder} singleton bean
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder;
    }

    /**
     * Swagger documentation bean instantiation
     * @return {@link Docket} singleton bean
     */
    @Bean
    public Docket userApi() {
        // the api documentation is available at
        // {application URL}/swagger-ui.html
        return new Docket(DocumentationType.SWAGGER_2).select().paths(path -> path.startsWith("/user")).build();
    }
    
    /**
     * Declare message source singleton bean
     * @return {@link MessageSource} singleton bean
     */
    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasenames("classpath:/label_key_messages", "classpath:/webservice_validation_messages");
        messageSource.setUseCodeAsDefaultMessage(true);
        messageSource.setDefaultEncoding("UTF-8");
        messageSource.setCacheSeconds(-1);
        return messageSource;
    }

//    @Bean
//    public Executor taskScheduler() {
//        return Executors.newScheduledThreadPool(5);
//    }

    @Bean()
    public  ThreadPoolTaskScheduler taskScheduler(){
        ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
        taskScheduler.setPoolSize(2);
        return  taskScheduler;
    }
}

/**
 * This package contains all schedule executor classes.
 * @author Torti Ama-Njoku @ Black Swan
 */
package za.co.blackswan.microservice.users.scheduler;
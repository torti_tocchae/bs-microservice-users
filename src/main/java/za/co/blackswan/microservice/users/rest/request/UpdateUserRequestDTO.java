package za.co.blackswan.microservice.users.rest.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import za.co.blackswan.microservice.users.data.model.User;
import za.co.blackswan.microservice.users.type.UserStatusEnum;

/**
 * REST webservice request entity for {@link User} updates
 * @author Torti Ama-Njoku @ Black Swan
 */
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class UpdateUserRequestDTO {

    private String firstName;

    private String lastName;

    private UserStatusEnum status;
    
    /**
     * Default Constructor
     */
    public UpdateUserRequestDTO() {
        super();
    }

    /**
     * Gets lastName.
     *
     * @return Value of lastName.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Gets firstName.
     *
     * @return Value of firstName.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets new lastName.
     *
     * @param lastName New value of lastName.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Sets new status.
     *
     * @param status New value of status.
     */
    public void setStatus(UserStatusEnum status) {
        this.status = status;
    }

    /**
     * Sets new firstName.
     *
     * @param firstName New value of firstName.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets status.
     *
     * @return Value of status.
     */
    public UserStatusEnum getStatus() {
        return status;
    }
}

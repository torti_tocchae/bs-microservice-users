package za.co.blackswan.microservice.users.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import za.co.blackswan.microservice.users.data.model.User;

/**
 * This is the data access interface used for retrieving and searching the {@link User}
 * entity table for access to the {@link User} data.
 * 
 * @author Torti Ama-Njoku @ Black Swan
 */

@Repository
public interface UserDao extends JpaRepository<User, Long> {

    /**
     * Find a {@link User} with the given username
     * @param username username to search for
     * @return {@User} that has been retrieved
     */
    @Query("select u from User u where username = ?1")
    User findUserByUsername(String username);
}

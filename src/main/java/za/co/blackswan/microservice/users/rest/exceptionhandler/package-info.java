/**
 * This package contains REST exception handlers.
 * 
 * @since 1.0
 * @author Torti Ama-Njoku @ Black Swan
 * @version 1.0
 */
package za.co.blackswan.microservice.users.rest.exceptionhandler;

package za.co.blackswan.microservice.users.controller;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import za.co.blackswan.microservice.users.data.model.User;
import za.co.blackswan.microservice.users.exception.AlreadyExistsException;
import za.co.blackswan.microservice.users.exception.EntityNotFoundException;
import za.co.blackswan.microservice.users.exception.MandatoryDetailsException;
import za.co.blackswan.microservice.users.rest.request.CreateUserRequestDTO;
import za.co.blackswan.microservice.users.rest.request.UpdateUserRequestDTO;
import za.co.blackswan.microservice.users.rest.response.UserListResponseDTO;
import za.co.blackswan.microservice.users.rest.response.UserResponseDTO;
import za.co.blackswan.microservice.users.service.UserService;
import za.co.blackswan.microservice.users.type.ResponseEnum;

/**
 * API Class that holds all the webservices for {@link User} operations
 * @author Torti Ama-Njoku @ Black Swan
 */
@RestController
@RequestMapping("/user")
@Transactional
@Validated
public class UserApiController {

    // Define the logger object for this class
    private static final Logger LOGGER = LoggerFactory.getLogger(UserApiController.class);
    private static final Locale LOCALE = new Locale.Builder().setLanguage("en").setRegion("ZA").build();

    private final UserService userService;
    private final MessageSource messageSource;
    
    /**
     * Spring will autowire the parameters of this method by argument
     * resolvers the parameters in this constructor.
     * 
     * @param userService {@link User} service singleton used to operate on the {@link User} data
     * @param messageSource source for configured messages
     */
    public UserApiController(final UserService userService, final MessageSource messageSource) {
        this.userService = userService;
        this.messageSource = messageSource;
    }
    
    /**
     * Webservice API called when a {@link User} is created
     * @param dataHolderRequest {@link CreateUserRequestDTO} webservice rest input object
     * @return {@link ResponseEntity} HTTP response entity with JSON response attributes
     * @throws AlreadyExistsException when {@link User} with the same username already exists
     * @throws MandatoryDetailsException if some mandatory details are missing
     * @throws EntityNotFoundException
     */
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<UserResponseDTO> createUser(
            @Valid @RequestBody final CreateUserRequestDTO dataHolderRequest)
            throws AlreadyExistsException, MandatoryDetailsException {

        LOGGER.debug("Calling createUser");
        
        // Calling the user creation method
        User createdUser = userService.createUser(dataHolderRequest.getFirstName(),
                dataHolderRequest.getLastName(), dataHolderRequest.getUsername());
        
        // Build response object
        UserResponseDTO response = new UserResponseDTO(ResponseEnum.SUCCESSFUL.getCode(),
                messageSource.getMessage(ResponseEnum.SUCCESSFUL.getLabelKey(),
                        new Object[]{messageSource.getMessage("user.creation.successful", null, LOCALE)},
                        LOCALE),
                ResponseEnum.SUCCESSFUL.getHttpCode().value(), new Date().getTime(), createdUser);

        return new ResponseEntity<>(response, ResponseEnum.SUCCESSFUL.getHttpCode());
    }

    /**
     * Webservice API called when a {@link User} is updated
     * @param userId id of the {@link User} to be updated or created if it does not exist
     * @param dataHolderRequest {@link UpdateUserRequestDTO} webservice rest input object
     * @return {@link ResponseEntity} HTTP response entity with JSON response attributes
     * @throws EntityNotFoundException if the {@link User} to be updated cannot be found
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<UserResponseDTO> updateUser(
            @PathVariable(name = "id") Long userId,
            @Valid @RequestBody UpdateUserRequestDTO dataHolderRequest)
            throws EntityNotFoundException {

        LOGGER.debug("Calling user update");

        // update user
        User updatedUser = userService.updateUser(userId, dataHolderRequest.getFirstName(),
                    dataHolderRequest.getLastName(), dataHolderRequest.getStatus());

        // Build response object
        UserResponseDTO response = new UserResponseDTO(ResponseEnum.SUCCESSFUL.getCode(),
                messageSource.getMessage(ResponseEnum.SUCCESSFUL.getLabelKey(),
                        new Object[]{messageSource.getMessage("user.updateUser.successful", null, LOCALE)},
                        LOCALE),
                ResponseEnum.SUCCESSFUL.getHttpCode().value(), new Date().getTime(), updatedUser);

        return new ResponseEntity<>(response, ResponseEnum.SUCCESSFUL.getHttpCode());
    }
    
    /**
     * Webservice API called to get a list of all {@link User}s in the system
     * @return {@link ResponseEntity} HTTP response entity with JSON response attributes
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<UserListResponseDTO> getUsers() {

        LOGGER.debug("Calling getUsers");

        List<User> users = userService.getAllUsers();
        
        // Build response object
        UserListResponseDTO response = new UserListResponseDTO(ResponseEnum.SUCCESSFUL.getCode(),
                messageSource.getMessage(ResponseEnum.SUCCESSFUL.getLabelKey(),
                        new Object[]{messageSource.getMessage("user.getAllUsers.successful", null, LOCALE)},
                        LOCALE),
                ResponseEnum.SUCCESSFUL.getHttpCode().value(), new Date().getTime(), users);

        return new ResponseEntity<>(response, ResponseEnum.SUCCESSFUL.getHttpCode());
    }

    /**
     * Webservice API called to get details of a specific {@link User} in the system
     * @param userId primary identifier of the {@link User} to be retrieved
     * @return {@link ResponseEntity} HTTP response entity with JSON response attributes
     * @throws EntityNotFoundException if the {@link User} being searched for cannot be found
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<UserResponseDTO> getUser(@PathVariable(name = "id") Long userId)
            throws EntityNotFoundException {

        LOGGER.debug("Calling getUser");

        // Calling the find user method
        User user = userService.findById(userId);

        // Build response object
        UserResponseDTO response = new UserResponseDTO(ResponseEnum.SUCCESSFUL.getCode(),
                messageSource.getMessage(ResponseEnum.SUCCESSFUL.getLabelKey(),
                        new Object[]{messageSource.getMessage("user.getUser.successful", null, LOCALE)},
                        LOCALE),
                ResponseEnum.SUCCESSFUL.getHttpCode().value(), new Date().getTime(), user);

        return new ResponseEntity<>(response, ResponseEnum.SUCCESSFUL.getHttpCode());
    }
}

package za.co.blackswan.microservice.users.rest.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import java.util.Date;
import za.co.blackswan.microservice.users.data.model.Task;

/**
 * REST webservice request entity for updating {@link Task}s
 * @author Torti Ama-Njoku @ Black Swan
 */
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class UpdateTaskRequestDTO {

    private String name;

    private String description;

    private Date dateTime;

    /**
     * Default Constructor
     */
    public UpdateTaskRequestDTO() {
        super();
    }

    /**
     * Sets new name.
     *
     * @param name New value of name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets name.
     *
     * @return Value of name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets new description.
     *
     * @param description New value of description.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets description.
     *
     * @return Value of description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets dateTime.
     *
     * @return Value of dateTime.
     */
    public Date getDateTime() {
        return dateTime;
    }

    /**
     * Sets new dateTime.
     *
     * @param dateTime New value of dateTime.
     */
    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }
}

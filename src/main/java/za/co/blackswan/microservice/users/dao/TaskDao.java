package za.co.blackswan.microservice.users.dao;

import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import za.co.blackswan.microservice.users.data.model.Task;
import za.co.blackswan.microservice.users.data.model.User;

/**
 * This is the data access interface used for retrieving and searching the {@link Task}
 * entity table for access to the {@link Task} data.
 * 
 * @author Torti Ama-Njoku @ Black Swan
 */
@Repository
public interface TaskDao extends JpaRepository<Task, Long> {

    /**
     * Get a {@link User}'s {@link Task}s
     * @param userId identifier of the user to be searched for
     * @return list of all {@link User}'s {@link Task}s
     */
    @Query("select t from Task t where t.user.id = ?1")
    List<Task> getAllByUser(Long userId);

    /**
     * Gets a list of all pending and expired {@link Task}s
     * @param currentDate {@link Date} to be compared with
     * @return a list of all pending and expired {@link Task}s
     */
    @Query("select t from Task t where t.status = za.co.blackswan.microservice.users.type.TaskStatusEnum.PENDING and t.dateTime < ?1 ")
    List<Task> getAllPendingExpiredTasks(Date currentDate);
}

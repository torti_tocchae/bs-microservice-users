/**
 * This package contains service interfaces used to access/perform operations on the data.
 * 
 * @since 1.0
 * @author Torti Ama-Njoku @ Black Swan
 * @version 1.0
 */
package za.co.blackswan.microservice.users.service;

package za.co.blackswan.microservice.users.service.impl;

import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.co.blackswan.microservice.users.dao.UserDao;
import za.co.blackswan.microservice.users.data.model.User;
import za.co.blackswan.microservice.users.exception.AlreadyExistsException;
import za.co.blackswan.microservice.users.exception.EntityNotFoundException;
import za.co.blackswan.microservice.users.exception.MandatoryDetailsException;
import za.co.blackswan.microservice.users.service.UserService;
import za.co.blackswan.microservice.users.type.ResponseEnum;
import za.co.blackswan.microservice.users.type.UserStatusEnum;
import za.co.blackswan.microservice.users.util.PasswordGenerator;

/**
 * Implementation of {@link UserService} interface.
 * 
 * @author Torti Ama-Njoku @ Black Swan
 */
@Service("userService")
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
    private static final Locale LOCALE = new Locale.Builder().setLanguage("en").setRegion("ZA").build();

    private final PasswordEncoder passwordEncoder;

    private final UserDao userDao;

    private final MessageSource messageSource;

    /**
     * Used by spring to instantiate a bean of this class autowiring by argument
     * resolvers the parameters in this constructor.
     * 
     * @param passwordEncoder used to encode the password
     * @param userDao 
     * @param messageSource source for error messages
     */
    public UserServiceImpl(final PasswordEncoder passwordEncoder, final UserDao userDao,
            final MessageSource messageSource) {
        this.passwordEncoder = passwordEncoder;
        this.userDao = userDao;
        this.messageSource = messageSource;
    }
    
    @Transactional(readOnly = true)
    @Override
    public List<User> getAllUsers() {
        List<User> found = userDao.findAll();
        return found;
    }
    
    @Transactional(readOnly = true)
    @Override
    public User findById(Long id) throws EntityNotFoundException {
        Optional<User> found = userDao.findById(id);
        if (!found.isPresent()) {
            throw new EntityNotFoundException(
                //user.notFound.id
                messageSource.getMessage(ResponseEnum.RESPONSE_CODE_110.getLabelKey(),
                        new Object[] {id}, LOCALE),
                ResponseEnum.RESPONSE_CODE_110);
        }
        return found.get();
    }

    @Transactional(readOnly = true)
    @Override
    public User findUser(String username) throws EntityNotFoundException {
        User found = userDao.findUserByUsername(username);
        if (found == null) {
            throw new EntityNotFoundException(
                //user.notFound.username
                messageSource.getMessage(ResponseEnum.RESPONSE_CODE_111.getLabelKey(), 
                        new Object[] {username}, LOCALE),
                ResponseEnum.RESPONSE_CODE_111);
        }
        return found;
    }
    
    /**
     * Checks if a {@link User} already exists
     * @param username username to check for
     * @return true if the {@link User} exists or false if not
     */
    private boolean userExists(String username) {
        try {
            findUser(username);
            return true;
        } catch (EntityNotFoundException ex) {
            return false;
        }
    }

    /**
     * Saves a new {@link User} to the database
     * @param user {@link User} to be saved
     * @return saved {@link User} entity
     * @throws AlreadyExistsException is a {@link User} with the given username already exists
     * @throws MandatoryDetailsException if some mandatory details are missing
     */
    private User saveUser(User user) throws AlreadyExistsException, MandatoryDetailsException {

        // Check if there is any missing mandatory details, throw the error
        if (user.hasMissingDetails()) {
            throw new MandatoryDetailsException(
                //user.save.mandatoryDetails
                messageSource.getMessage(ResponseEnum.RESPONSE_CODE_112.getLabelKey(), 
                        null, LOCALE),
                ResponseEnum.RESPONSE_CODE_112);
        }
        
        // Check if the unique details is applicable to another user already
        if (userExists(user.getUsername())) {
            throw new AlreadyExistsException(
                //user.save.duplicate
                messageSource.getMessage(ResponseEnum.RESPONSE_CODE_113.getLabelKey(),
                        null, LOCALE),
                ResponseEnum.RESPONSE_CODE_113);
        }

        // Encode the password
        if (passwordEncoder != null) {
            // Check whether we have to encrypt (or re-encrypt) the password
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        } else {
            LOGGER.warn("PasswordEncoder not set, skipping password encryption...");
        }
        
        // Save the new user or user changes to the DB
        User savedUser = userDao.saveAndFlush(user);
        
        // Return the new user object
        return savedUser;
    }
    
    @Transactional
    @Override
    public User updateUser(Long userId, String firstName, String lastName, UserStatusEnum status)
            throws EntityNotFoundException {

        // Find the user to be updated
        User userToUpdate = findById(userId);

        // Update the user with the new details
        if (StringUtils.isNotBlank(firstName)) {
            userToUpdate.setFirstName(firstName);
        }

        if (StringUtils.isNotBlank(lastName)) {
            userToUpdate.setLastName(lastName);
        }

        if (Objects.nonNull(status)) {
            userToUpdate.setStatus(status);
        }
        
        // Save the new user or user changes to the DB
        User savedUser = userDao.saveAndFlush(userToUpdate);
        
        // Return the new user object
        return savedUser;
    }

    @Transactional
    @Override
    public User createUser(String firstName, String lastName, String username)
            throws AlreadyExistsException, MandatoryDetailsException {

        // Generate the user's random password
        String randomPassword = PasswordGenerator.getRandomSecurePassword();

        // Setup the user object to be saved
        User user = new User(firstName, lastName, username, randomPassword);
        
        // Save the user
        return saveUser(user);
    }

}
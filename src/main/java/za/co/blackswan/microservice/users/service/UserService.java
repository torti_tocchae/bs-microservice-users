package za.co.blackswan.microservice.users.service;

import java.util.List;
import za.co.blackswan.microservice.users.data.model.User;
import za.co.blackswan.microservice.users.exception.AlreadyExistsException;
import za.co.blackswan.microservice.users.exception.EntityNotFoundException;
import za.co.blackswan.microservice.users.exception.MandatoryDetailsException;
import za.co.blackswan.microservice.users.type.UserStatusEnum;

/**
 * Interface service class for executing desired {@link User} object operations
 * @author Torti Ama-Njoku @ Black Swan
 */
public interface UserService {
    
    /**
     * Get a list of all {@link User}s
     * @return retrieved list of {@link User} objects
     */
    List<User> getAllUsers();
    
    /**
     * Find a {@link User} by the {@link User} ID, i.e. the primary identifier in the table
     * @param id primary identifier value of the {@link User} to be found
     * @return retrieved {@link User} object
     * @throws EntityNotFoundException if the {@link User} is not found
     */
    User findById(Long id) throws EntityNotFoundException;

    /**
     * Find a {@link User} by the {@link User}'s unique username
     * @param username username of the {@link User} to be searched
     * @return retrieved {@link User} object
     * @throws EntityNotFoundException if the {@link User} is not found
     */
    User findUser(String username) throws EntityNotFoundException;

    /**
     * Update a {@link User}'s details
     * @param userId ID of the {@link User} to be updated
     * @param firstName new first name
     * @param lastName new last name
     * @param status new status
     * @return updated {@link User} object
     * @throws EntityNotFoundException if the {@link User} to be updated is not found
     */
    User updateUser(Long userId, String firstName, String lastName, UserStatusEnum status)
            throws EntityNotFoundException;
    
    /**
     * Create a new {@link User}
     * @param firstName {@link User}'s first name
     * @param lastName {@link User}'s last name
     * @param username unique username for this {@link User}
     * @return the created {@link User} entity
     * @throws AlreadyExistsException is a {@link User} with the given username already exists
     * @throws MandatoryDetailsException if some mandatory details are missing
     */
    User createUser(String firstName, String lastName, String username)
            throws AlreadyExistsException, MandatoryDetailsException;
}
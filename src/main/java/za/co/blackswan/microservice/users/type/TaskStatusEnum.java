package za.co.blackswan.microservice.users.type;

import za.co.blackswan.microservice.users.data.model.Task;

/**
 * Enum class to hold {@link Task} statuses
 * 
 * @author Torti Ama-Njoku @ Black Swan
 */
public enum TaskStatusEnum {

    /**
     * Active {@link Task} status
     */
    PENDING("Pending"),
    /**
     * An inactive {@link Task} status
     */
    DONE("Done");

    private final String labelKey;

    /**
     * Initialisation
     *
     * @param labelKey replacement string
     */
    TaskStatusEnum(String labelKey) {
            this.labelKey = labelKey;
    }
    
    /**
     * {@link TaskStatusEnum} String representation
     *
     * @return String
     */
    public String getKeyValue() {
            return labelKey;
    }
}

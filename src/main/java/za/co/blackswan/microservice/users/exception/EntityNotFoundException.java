package za.co.blackswan.microservice.users.exception;

import za.co.blackswan.microservice.users.type.ResponseEnum;

/**
 * Exception class to define exception for when an object cannot be found in the database.
 *
 * @author Torti Ama-Njoku @ Black Swan
 */
@SuppressWarnings("serial")
public class EntityNotFoundException extends MicroserviceException {

    /**
     * Default Constructor
     */
    public EntityNotFoundException() { }

    /**
     * Constructor to with exception message
     * @param message exception message
     * @param exceptionResponse the response object associated with this exception
     */
    public EntityNotFoundException(String message, ResponseEnum exceptionResponse) {
        super(message, exceptionResponse);
    }

    /**
     * Constructor with message and exception cause
     * @param message exception message
     * @param cause exception cause
     * @param exceptionResponse the response object associated with this exception
     */
    public EntityNotFoundException(String message, Throwable cause, ResponseEnum exceptionResponse) {
        super(message, cause, exceptionResponse);
    }
}

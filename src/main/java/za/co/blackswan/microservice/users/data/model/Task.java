package za.co.blackswan.microservice.users.data.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import za.co.blackswan.microservice.users.type.TaskStatusEnum;

/**
 * This class is used to represent users' tasks on the database
 *
 * @author Torti Ama-Njoku @ Black Swan
 */
@Entity
@Table(name = "bs_user_task")
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Task implements Serializable, Comparable<Task> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @Column(name = "name", nullable = false, length = 1000)
    private String name;

    @Column(name = "description", nullable = false, length = 2000)
    private String description;

    @Column(name = "date_time", nullable = false, length = 20)
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date dateTime;

    @Column(name = "status", nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    private TaskStatusEnum status;

    /**
     * Default Constructor
     */
    public Task() {
        super();
    }

    /**
     * Create a new task with applicable attributes/properties
     * @param user user the task belongs to
     * @param name name of the task
     * @param description description of the task/appropriate comment
     * @param dateTime date and time the task is scheduled for
     */
    public Task(User user, String name, String description, Date dateTime) {
        this.user = user;
        this.name = name;
        this.description = description;
        this.dateTime = dateTime;
        this.status = TaskStatusEnum.PENDING;
    }

    /**
     * Create a task all attributes/properties
     * @param id unique identifier of the task
     * @param user user the task belongs to
     * @param name name of the task
     * @param description description of the task/appropriate comment
     * @param dateTime date and time the task is scheduled for
     * @param status status of the {@link Task}
     */
    public Task(Long id, User user, String name, String description, Date dateTime, TaskStatusEnum status) {
        this.id = id;
        this.user = user;
        this.name = name;
        this.description = description;
        this.dateTime = dateTime;
        this.status = status;
    }

    /**
     * Sets new id.
     *
     * @param id New value of id.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets id.
     *
     * @return Value of id.
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets new user.
     *
     * @param user New value of user.
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Gets user.
     *
     * @return Value of user.
     */
    public User getUser() {
        return user;
    }

    /**
     * Gets name.
     *
     * @return Value of name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets new name.
     *
     * @param name New value of name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets new description.
     *
     * @param description New value of description.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets description.
     *
     * @return Value of description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets new dateTime.
     *
     * @param dateTime New value of dateTime.
     */
    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    /**
     * Gets dateTime.
     *
     * @return Value of dateTime.
     */
    public Date getDateTime() {
        return dateTime;
    }

    /**
     * Sets new status.
     *
     * @param status New value of status.
     */
    public void setStatus(TaskStatusEnum status) {
        this.status = status;
    }

    /**
     * Gets status.
     *
     * @return Value of status.
     */
    public TaskStatusEnum getStatus() {
        return status;
    }

    /**
     * Check if this task has been saved to the DB already
     * @return true if it has and false if not
     */
    @Transient
    @JsonIgnore
    public boolean isPersisted() {
        return getId() != null;
    }

    /**
     * Check if there are any missing mandatory details in the user object
     * @return true if there are any missing mandatory details and false if not
     */
    @JsonIgnore
    public boolean hasMissingDetails() {
        return StringUtils.isAnyBlank(getName(), getDescription()) || Objects.isNull(getUser()) || Objects.isNull(getDateTime());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Task rhs = (Task) obj;
        return new EqualsBuilder()
                        .append(getId(), rhs.getId())
                        .isEquals();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(11, 31)
                        .append(getId())
                        .append(getUser().getId())
                        .append(getName())
                        .toHashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

    @Override
    public int compareTo(Task o) {
        return new CompareToBuilder().append(this.getId(), o.getId()).toComparison();
    }
}

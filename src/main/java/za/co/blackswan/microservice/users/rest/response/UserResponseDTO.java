package za.co.blackswan.microservice.users.rest.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import za.co.blackswan.microservice.users.data.model.User;

/**
 * REST webservice response entity for {@link User} details
 *
 * @author Torti Ama-Njoku @ Black Swan
 */
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class UserResponseDTO extends GenericWSResponseDTO {

    private User user;
    
    /**
     * Default Constructor
     */
    public UserResponseDTO() {
        super();
    }
    
    /**
     * @param responseCode webservice response code
     * @param responseMessage webservice response message
     * @param status HTTP status code
     * @param timeStamp time stamp of the reponse
     */
    public UserResponseDTO(String responseCode, String responseMessage,
                           int status, long timeStamp) {
        super(responseCode, responseMessage, status, timeStamp);
    }

    /**
     * @param responseCode webservice response code
     * @param responseMessage webservice response message
     * @param status HTTP status code
     * @param timeStamp time stamp of the reponse
     * @param user to be returned
     */
    public UserResponseDTO(String responseCode, String responseMessage,
                           int status, long timeStamp, User user) {
        super(responseCode, responseMessage, status, timeStamp);
        this.user = user;
    }

    /**
     * Sets new user.
     *
     * @param user New value of user.
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Gets user.
     *
     * @return Value of user.
     */
    public User getUser() {
        return user;
    }
}

package za.co.blackswan.microservice.users;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Spring Boot Application Runner
 * @author Torti Ama-Njoku @ Black Swan
 */
@SpringBootApplication
@EnableScheduling
public class UsersMicroserviceApplication {

    /**
     * Runtime method executed when application is started
     * @param args command line arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(UsersMicroserviceApplication.class, args);
    }
}

package za.co.blackswan.microservice.users.data.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import za.co.blackswan.microservice.users.type.UserStatusEnum;

/**
 * This class is used to represent available users on the database
 *
 * @author Torti Ama-Njoku @ Black Swan
 */
@Entity
@Table(name = "bs_user", uniqueConstraints = {
    @UniqueConstraint(name = "uq_usr_username", columnNames = {"username"})})
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class User implements Serializable, Comparable<User> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "first_name", length = 1000, nullable = false)
    private String firstName;

    @Column(name = "last_name", length = 1000, nullable = false)
    private String lastName;

    @Column(name = "status", nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    private UserStatusEnum status;

    @Column(name = "username", length = 20, nullable = false)
    private String username;

    @Column(name = "password", length = 100, nullable = false)
    @JsonIgnore
    private String password;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    @JsonIgnore
    private Set<Task> tasks = new HashSet<>();
    
    /**
     * Default constructor - creates a new instance with no values set.
     */
    public User() {
        super();
    }
    
    /**
     * Create User with all applicable attributes
     * 
     * @param firstName first name
     * @param lastName last name
     * @param username username
     * @param password login password
     */
    public User(String firstName, String lastName, String username, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.status = UserStatusEnum.ACTIVATED;
        this.username = username;
        this.password = password;
    }
    
    /**
     * Create a user object with all the attributes
     * @param id primary identifier
     * @param firstName first name
     * @param lastName last name
     * @param status user's status {@link UserStatusEnum}
     * @param username unique identifier
     * @param password 
     */
    public User(Long id, String firstName, String lastName, UserStatusEnum status, String username, String password) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.status = status;
        this.username = username;
        this.password = password;
    }

    /**
     * Gets id.
     *
     * @return Value of id.
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets new id.
     *
     * @param id New value of id.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Sets new firstName.
     *
     * @param firstName New value of firstName.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets firstName.
     *
     * @return Value of firstName.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Gets lastName.
     *
     * @return Value of lastName.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets new lastName.
     *
     * @param lastName New value of lastName.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets status.
     *
     * @return Value of status.
     */
    public UserStatusEnum getStatus() {
        return status;
    }

    /**
     * Sets new status.
     *
     * @param status New value of status.
     */
    public void setStatus(UserStatusEnum status) {
        this.status = status;
    }

    /**
     * Gets username.
     *
     * @return Value of username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets new username.
     *
     * @param username New value of username.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Sets new password.
     *
     * @param password New value of password.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets password.
     *
     * @return Value of password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Gets tasks.
     *
     * @return Value of tasks.
     */
    public Set<Task> getTasks() {
        return tasks;
    }

    /**
     * Sets new tasks.
     *
     * @param tasks New value of tasks.
     */
    public void setTasks(Set<Task> tasks) {
        this.tasks = tasks;
    }
    
    /**
     * Check if this user has been saved to the DB already
     * @return true if it has and false if not
     */
    @Transient
    @JsonIgnore
    public boolean isPersisted() {
        return getId() != null;
    }
    
    /**
     * Check if there are any missing mandatory details in the user object
     * @return true if there are any missing mandatory details and false if not
     */
    @JsonIgnore
    public boolean hasMissingDetails() {
        return StringUtils.isAnyBlank(getFirstName(), getLastName(), getUsername(), getPassword()) || Objects.isNull(getStatus());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        User rhs = (User) obj;
        return new EqualsBuilder().append(getUsername(), rhs.getUsername()).isEquals();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(11, 31).append(getUsername()).toHashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

    @Override
    public int compareTo(User o) {
        return new CompareToBuilder().append(this.getId(), o.getId()).toComparison();
    }
}

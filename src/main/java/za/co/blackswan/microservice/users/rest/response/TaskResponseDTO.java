package za.co.blackswan.microservice.users.rest.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import za.co.blackswan.microservice.users.data.model.Task;

/**
 * REST webservice response entity for {@link Task} details
 *
 * @author Torti Ama-Njoku @ Black Swan
 */
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class TaskResponseDTO extends GenericWSResponseDTO {

    private Task task;

    /**
     * Default Constructor
     */
    public TaskResponseDTO() {
        super();
    }

    /**
     * @param responseCode webservice response code
     * @param responseMessage webservice response message
     * @param status HTTP status code
     * @param timeStamp time stamp of the reponse
     */
    public TaskResponseDTO(String responseCode, String responseMessage,
                           int status, long timeStamp) {
        super(responseCode, responseMessage, status, timeStamp);
    }

    /**
     * @param responseCode webservice response code
     * @param responseMessage webservice response message
     * @param status HTTP status code
     * @param timeStamp time stamp of the reponse
     * @param task to be returned
     */
    public TaskResponseDTO(String responseCode, String responseMessage,
                           int status, long timeStamp, Task task) {
        super(responseCode, responseMessage, status, timeStamp);
        this.task = task;
    }

    /**
     * Sets new task.
     *
     * @param task New value of task.
     */
    public void setTask(Task task) {
        this.task = task;
    }

    /**
     * Gets task.
     *
     * @return Value of task.
     */
    public Task getTask() {
        return task;
    }
}

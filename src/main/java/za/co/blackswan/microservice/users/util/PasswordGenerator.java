package za.co.blackswan.microservice.users.util;

import java.security.SecureRandom;
import org.apache.commons.lang3.RandomStringUtils;

/**
 * A random password generator utility class.
 *
 * @author Torti Ama-Njoku
 */
public final class PasswordGenerator {

    private static final char[] POSSIBLECHARACTERS =
            new String("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]}\\|;:\'\",<.>/?")
            .toCharArray();

    private static final int RANDOMPASSWORDLENGTH = 10;

    /**
     * Defeat Instantiation
     */
    private PasswordGenerator() { }

    /**
     * Generates a secure random password.
     * @return
     */
    public static String getRandomSecurePassword() {
        return RandomStringUtils.random(
                    RANDOMPASSWORDLENGTH, 0,
                    POSSIBLECHARACTERS.length-1,
                    false, false,
                    POSSIBLECHARACTERS, new SecureRandom());
    }
}

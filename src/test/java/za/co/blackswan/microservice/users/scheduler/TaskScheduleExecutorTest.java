package za.co.blackswan.microservice.users.scheduler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import za.co.blackswan.microservice.users.UsersMicroserviceApplication;
import za.co.blackswan.microservice.users.controller.TaskApiController;
import za.co.blackswan.microservice.users.dao.TaskDao;
import za.co.blackswan.microservice.users.dao.UserDao;
import za.co.blackswan.microservice.users.data.model.Task;
import za.co.blackswan.microservice.users.data.model.User;
import za.co.blackswan.microservice.users.service.TaskService;
import za.co.blackswan.microservice.users.service.UserService;
import za.co.blackswan.microservice.users.type.TaskStatusEnum;

/**
 * TaskScheduleExecutor test
 * @author Torti Ama-Njoku
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = UsersMicroserviceApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TaskScheduleExecutorTest {
    @LocalServerPort
    private int port;

    private User testUser1;

    @Autowired
    UserService userService;

    @Autowired
    TaskService taskService;

    @Autowired
    UserDao userDao;

    @Autowired
    TaskDao taskDao;

    /**
     * Test of {@link TaskApiController#createTask} method.
     */
    @Test
    public void test1_RunSchedule() throws Exception {

        // Setup the DB with data
        saveTestUsers();

        taskService.createTask(testUser1.getId(), "Test Task1", "Test Description1", DateUtils.addDays(new Date(), -1));
        taskService.createTask(testUser1.getId(), "Test Task2", "Test Description2", DateUtils.addDays(new Date(), -2));
        taskService.createTask(testUser1.getId(), "Test Task3", "Test Description3", DateUtils.addDays(new Date(), -3));

        List<Task> tasks = taskService.getAllUserTasks(testUser1.getId());

        assertEquals(3, tasks.size());
        assertTrue(tasks.get(0).getStatus().equals(TaskStatusEnum.PENDING));
        assertTrue(tasks.get(1).getStatus().equals(TaskStatusEnum.PENDING));
        assertTrue(tasks.get(2).getStatus().equals(TaskStatusEnum.PENDING));

        TaskScheduleExecutor taskScheduleExecutor = new TaskScheduleExecutor(taskService);
        taskScheduleExecutor.processAllPendingTasks();

        tasks = taskService.getAllUserTasks(testUser1.getId());

        assertEquals(3, tasks.size());
        assertTrue(tasks.get(0).getStatus().equals(TaskStatusEnum.DONE));
        assertTrue(tasks.get(1).getStatus().equals(TaskStatusEnum.DONE));
        assertTrue(tasks.get(2).getStatus().equals(TaskStatusEnum.DONE));

        taskDao.deleteAll();
        taskDao.flush();
        userDao.deleteAll();
        userDao.flush();
    }

    /**
     * Save a set of test users
     */
    private void saveTestUsers() throws Exception {
        testUser1 = userService.createUser("Test1", "User1", "testUser1");
    }
}
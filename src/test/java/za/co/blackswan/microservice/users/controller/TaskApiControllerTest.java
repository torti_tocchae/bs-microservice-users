package za.co.blackswan.microservice.users.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import za.co.blackswan.microservice.users.UsersMicroserviceApplication;
import za.co.blackswan.microservice.users.dao.TaskDao;
import za.co.blackswan.microservice.users.dao.UserDao;
import za.co.blackswan.microservice.users.data.model.Task;
import za.co.blackswan.microservice.users.data.model.User;
import za.co.blackswan.microservice.users.rest.request.CreateTaskRequestDTO;
import za.co.blackswan.microservice.users.rest.request.UpdateTaskRequestDTO;
import za.co.blackswan.microservice.users.rest.response.TaskListResponseDTO;
import za.co.blackswan.microservice.users.rest.response.TaskResponseDTO;
import za.co.blackswan.microservice.users.service.TaskService;
import za.co.blackswan.microservice.users.service.UserService;

/**
 * TaskApiController Test
 * @author Torti Ama-Njoku @ UXP
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = UsersMicroserviceApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TaskApiControllerTest {
    
    @LocalServerPort
    private int port;

    private TestRestTemplate restTemplate;

    private User testUser1;
    private User testUser2;
    private User testUser3;
    
    @Autowired
    UserService userService;
    
    @Autowired
    UserDao userDao;

    @Autowired
    TaskService taskService;

    @Autowired
    TaskDao taskDao;
    
    @Before
    public void setUp() throws Exception {
        restTemplate = new TestRestTemplate();
    }

    /**
     * Test of {@link TaskApiController#createTask} method.
     */
    @Test
    public void test1_CreateTask() throws Exception {
        
        // Setup the DB with data
        saveTestUsers();
        
        // Test for when first name is empty
        CreateTaskRequestDTO dataHolderRequest = new CreateTaskRequestDTO();
        dataHolderRequest.setDescription("Test Description");
        dataHolderRequest.setDateTime(new Date());
        
        TaskResponseDTO response = callCreateTaskEndpoint(dataHolderRequest, testUser1.getId());
        
        assertEquals(400, response.getStatus()); // Should return HTTP Status BAD_REQUEST

        dataHolderRequest = new CreateTaskRequestDTO();
        dataHolderRequest.setName(" ");
        dataHolderRequest.setDescription("Test Description");
        dataHolderRequest.setDateTime(new Date());

        response = callCreateTaskEndpoint(dataHolderRequest, testUser1.getId());

        assertEquals(206, response.getStatus()); // Should return HTTP Status PARTIAL_CONTENT

        // Test for when description is empty
        dataHolderRequest = new CreateTaskRequestDTO();
        dataHolderRequest.setName("Test Task");
        dataHolderRequest.setDateTime(new Date());

        response = callCreateTaskEndpoint(dataHolderRequest, testUser1.getId());

        assertEquals(400, response.getStatus()); // Should return HTTP Status BAD_REQUEST

        // Test for when date time is empty
        dataHolderRequest = new CreateTaskRequestDTO();
        dataHolderRequest.setName("Test Task");
        dataHolderRequest.setDescription("Test Description");

        response = callCreateTaskEndpoint(dataHolderRequest, testUser2.getId());

        assertEquals(400, response.getStatus()); // Should return HTTP Status BAD_REQUEST

        // Test for when description is empty
        dataHolderRequest = new CreateTaskRequestDTO();
        dataHolderRequest.setName("Test Task");
        dataHolderRequest.setDateTime(new Date());

        response = callCreateTaskEndpoint(dataHolderRequest, testUser2.getId());

        assertEquals(400, response.getStatus()); // Should return HTTP Status BAD_REQUEST

        dataHolderRequest = new CreateTaskRequestDTO();
        dataHolderRequest.setName("Test Task");
        dataHolderRequest.setDescription(" ");
        dataHolderRequest.setDateTime(new Date());

        response = callCreateTaskEndpoint(dataHolderRequest, testUser2.getId());

        assertEquals(206, response.getStatus()); // Should return HTTP Status PARTIAL_CONTENT

        // Test successful task creation
        dataHolderRequest = new CreateTaskRequestDTO();
        dataHolderRequest.setName("Test Task");
        dataHolderRequest.setDescription("Test Task Description");
        dataHolderRequest.setDateTime(new Date());

        response = callCreateTaskEndpoint(dataHolderRequest, testUser1.getId());

        assertEquals(200, response.getStatus()); // Should return HTTP Status OK

        // Clear the DB
        taskDao.deleteAll();
        taskDao.flush();
        userDao.deleteAll();
        userDao.flush();
        
    }

    /**
     * Test of {@link TaskApiController#updateTask) method.
     */
    @Test
    public void test2_UpdateTask() throws Exception {

        // Setup the DB with data
        saveTestUsers();

        // Create a task
        CreateTaskRequestDTO dataHolderRequest = new CreateTaskRequestDTO();
        dataHolderRequest.setName("Test Task");
        dataHolderRequest.setDescription("Test Task Description");
        dataHolderRequest.setDateTime(new Date());

        TaskResponseDTO response = callCreateTaskEndpoint(dataHolderRequest, testUser3.getId());
        Task task = response.getTask();

        // Update the description and date
        UpdateTaskRequestDTO updateTaskRequestDTO = new UpdateTaskRequestDTO();
        updateTaskRequestDTO.setDescription("My new task description");
        updateTaskRequestDTO.setDateTime(DateUtils.addDays(task.getDateTime(), 5));

        response = callUpdateTaskEndpoint(updateTaskRequestDTO, testUser3.getId(), task.getId());

        assertEquals(200, response.getStatus()); // Should return HTTP Status OK
        assertEquals(updateTaskRequestDTO.getDescription(), response.getTask().getDescription());
        assertEquals(updateTaskRequestDTO.getDateTime(), response.getTask().getDateTime());
        assertEquals(task.getName(), response.getTask().getName()); // Task name should stay the same

        // Update a task that does not belong to the given user
        updateTaskRequestDTO = new UpdateTaskRequestDTO();
        updateTaskRequestDTO.setDescription("My new task description");
        updateTaskRequestDTO.setDateTime(DateUtils.addDays(task.getDateTime(), 5));

        response = callUpdateTaskEndpoint(updateTaskRequestDTO, testUser2.getId(), task.getId());

        assertEquals(409, response.getStatus()); // Should return HTTP Status CONFLICT

        // Clear the DB
        taskDao.deleteAll();
        taskDao.flush();
        userDao.deleteAll();
        userDao.flush();

    }

    /**
     * Test of {@link TaskApiController#deleteTask) method.
     */
    @Test
    public void test3_DeleteTask() throws Exception {

        // Setup the DB with data
        saveTestUsers();

        // Create a task
        CreateTaskRequestDTO dataHolderRequest = new CreateTaskRequestDTO();
        dataHolderRequest.setName("Test Task");
        dataHolderRequest.setDescription("Test Task Description");
        dataHolderRequest.setDateTime(new Date());

        TaskResponseDTO response = callCreateTaskEndpoint(dataHolderRequest, testUser2.getId());
        Task task = response.getTask();

        assertEquals(task, taskDao.findById(task.getId()).get());

        response = callDeleteTaskEndpoint(testUser3.getId(), task.getId());

        assertEquals(409, response.getStatus()); // Should return HTTP Status CONFLICT

        response = callDeleteTaskEndpoint(testUser2.getId(), task.getId());

        assertEquals(200, response.getStatus()); // Should return HTTP Status OK
        assertFalse(taskDao.findById(task.getId()).isPresent());

        // Clear the DB
        taskDao.deleteAll();
        taskDao.flush();
        userDao.deleteAll();
        userDao.flush();

    }

    /**
     * Test of {@link TaskApiController#getTask) method.
     */
    @Test
    public void test4_GetTask() throws Exception {

        // Setup the DB with data
        saveTestUsers();

        // Create a task
        CreateTaskRequestDTO dataHolderRequest = new CreateTaskRequestDTO();
        dataHolderRequest.setName("Test Task");
        dataHolderRequest.setDescription("Test Task Description");
        dataHolderRequest.setDateTime(new Date());

        TaskResponseDTO response = callCreateTaskEndpoint(dataHolderRequest, testUser2.getId());
        Task task = response.getTask();

        response = callGetTaskEndpoint(testUser3.getId(), task.getId());

        assertEquals(409, response.getStatus()); // Should return HTTP Status CONFLICT

        response = callGetTaskEndpoint(testUser2.getId(), task.getId());

        assertEquals(200, response.getStatus()); // Should return HTTP Status OK
        assertEquals(task, response.getTask());

        // Clear the DB
        taskDao.deleteAll();
        taskDao.flush();
        userDao.deleteAll();
        userDao.flush();
    }

    /**
     * Test of {@link TaskApiController#getTasks) method.
     */
    @Test
    public void test5_GetTasks() throws Exception {

        // Setup the DB with data
        saveTestUsers();

        // Create a task
        CreateTaskRequestDTO dataHolderRequest = new CreateTaskRequestDTO();
        dataHolderRequest.setName("Test Task");
        dataHolderRequest.setDescription("Test Task Description");
        dataHolderRequest.setDateTime(new Date());

        TaskResponseDTO response = callCreateTaskEndpoint(dataHolderRequest, testUser2.getId());
        Task task1 = response.getTask();

        dataHolderRequest.setName("Test Task2");
        dataHolderRequest.setDescription("Test Task Description2");
        dataHolderRequest.setDateTime(new Date());

        response = callCreateTaskEndpoint(dataHolderRequest, testUser2.getId());
        Task task2 = response.getTask();

        dataHolderRequest.setName("Test Task3");
        dataHolderRequest.setDescription("Test Task Description3");
        dataHolderRequest.setDateTime(new Date());

        response = callCreateTaskEndpoint(dataHolderRequest, testUser3.getId());
        Task task3 = response.getTask();

        TaskListResponseDTO taskListResponse = callGetTasksEndpoint(testUser2.getId());

        assertEquals(200, response.getStatus()); // Should return HTTP Status OK
        assertEquals(2, taskListResponse.getTasks().size());
        assertTrue(taskListResponse.getTasks().contains(task1));
        assertTrue(taskListResponse.getTasks().contains(task2));

        taskListResponse = callGetTasksEndpoint(testUser3.getId());

        assertEquals(200, response.getStatus()); // Should return HTTP Status OK
        assertEquals(1, taskListResponse.getTasks().size());
        assertTrue(taskListResponse.getTasks().contains(task3));

        // Clear the DB
        taskDao.deleteAll();
        taskDao.flush();
        userDao.deleteAll();
        userDao.flush();
    }
    
    /**
     * Save a set of test users
     */
    private void saveTestUsers() throws Exception {
        testUser1 = userService.createUser("Test1", "User1", "testUser1");
        testUser2 = userService.createUser("Test2", "User2", "testUser2");
        testUser3 = userService.createUser("Test3", "User3", "testUser3");
    }
    
    private TaskResponseDTO callCreateTaskEndpoint(CreateTaskRequestDTO dataHolderRequest, Long userId) throws Exception{
        HttpEntity<CreateTaskRequestDTO> entity = new HttpEntity<>(dataHolderRequest);
        ResponseEntity<TaskResponseDTO> response = restTemplate.exchange(
                        createURLWithPort("/api/user/" + userId + "/task"),
                        HttpMethod.POST, entity, TaskResponseDTO.class);
        
        return response.getBody();
    }

    private TaskResponseDTO callUpdateTaskEndpoint(UpdateTaskRequestDTO dataHolderRequest, Long userId, Long taskId) throws Exception{
        HttpEntity<UpdateTaskRequestDTO> entity = new HttpEntity<>(dataHolderRequest);
        ResponseEntity<TaskResponseDTO> response = restTemplate.exchange(
                createURLWithPort("/api/user/" + userId + "/task/" + taskId),
                HttpMethod.PUT, entity, TaskResponseDTO.class);

        return response.getBody();
    }

    private TaskResponseDTO callDeleteTaskEndpoint(Long userId, Long taskId) throws Exception{
        ResponseEntity<TaskResponseDTO> response = restTemplate.exchange(
                createURLWithPort("/api/user/" + userId + "/task/" + taskId),
                HttpMethod.DELETE, null, TaskResponseDTO.class);

        return response.getBody();
    }

    private TaskResponseDTO callGetTaskEndpoint(Long userId, Long taskId) throws Exception{
        ResponseEntity<TaskResponseDTO> response = restTemplate.exchange(
                createURLWithPort("/api/user/" + userId + "/task/" + taskId),
                HttpMethod.GET, null, TaskResponseDTO.class);

        return response.getBody();
    }

    private TaskListResponseDTO callGetTasksEndpoint(Long userId) throws Exception{
        ResponseEntity<TaskListResponseDTO> response = restTemplate.exchange(
                createURLWithPort("/api/user/" + userId + "/task"),
                HttpMethod.GET, null, TaskListResponseDTO.class);

        return response.getBody();
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }
}

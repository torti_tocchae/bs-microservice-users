package za.co.blackswan.microservice.users.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import za.co.blackswan.microservice.users.UsersMicroserviceApplication;
import za.co.blackswan.microservice.users.dao.UserDao;
import za.co.blackswan.microservice.users.data.model.User;
import za.co.blackswan.microservice.users.rest.request.CreateUserRequestDTO;
import za.co.blackswan.microservice.users.rest.request.UpdateUserRequestDTO;
import za.co.blackswan.microservice.users.rest.response.UserListResponseDTO;
import za.co.blackswan.microservice.users.rest.response.UserResponseDTO;
import za.co.blackswan.microservice.users.service.UserService;
import za.co.blackswan.microservice.users.type.UserStatusEnum;

/**
 * UserApiController Test 
 * @author Torti Ama-Njoku @ UXP
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = UsersMicroserviceApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserApiControllerTest {
    
    @LocalServerPort
    private int port;

    private TestRestTemplate restTemplate;

    private User testUser;
    
    @Autowired
    UserService userService;
    
    @Autowired
    UserDao userDao;
    
    @Before
    public void setUp() throws Exception {
        restTemplate = new TestRestTemplate();
    }

    /**
     * Test of createUser method, of class UserApiController.
     */
    @Test
    public void test1_CreateUser() throws Exception {
        
        // Setup the DB with data
        saveTestUsers();
        
        // Test for when first name is blank
        CreateUserRequestDTO dataHolderRequest = new CreateUserRequestDTO();
        dataHolderRequest.setFirstName("");
        dataHolderRequest.setLastName("Ama");
        dataHolderRequest.setUsername("testUser5");
        
        UserResponseDTO response = callCreateUserEndpoint(dataHolderRequest);
        
        assertEquals(206, response.getStatus()); // Should return HTTP Status PARTIAL_CONTENT
        assertEquals("112", response.getResponseCode());
        
        // Test for when last name is blank
        dataHolderRequest.setFirstName("Torti");
        dataHolderRequest.setLastName("");
        dataHolderRequest.setUsername("testUser5");
        
        response = callCreateUserEndpoint(dataHolderRequest);
        
        assertEquals(206, response.getStatus()); // Should return HTTP Status PARTIAL_CONTENT
        assertEquals("112", response.getResponseCode());

        // Test for when username is blank
        dataHolderRequest.setFirstName("Torti");
        dataHolderRequest.setLastName("Ama");
        dataHolderRequest.setUsername("");

        response = callCreateUserEndpoint(dataHolderRequest);

        assertEquals(206, response.getStatus()); // Should return HTTP Status PARTIAL_CONTENT
        assertEquals("112", response.getResponseCode());

        // Test when saving a new user, but for a username that exists
        dataHolderRequest.setFirstName("Torti");
        dataHolderRequest.setLastName("Ama");
        dataHolderRequest.setUsername("testUser1");;
        
        response = callCreateUserEndpoint(dataHolderRequest);
        
        assertEquals(409, response.getStatus()); // Should return HTTP Status CONFLICT
        assertEquals("113", response.getResponseCode());
        
        // Test a successful creation
        dataHolderRequest.setFirstName("Torti");
        dataHolderRequest.setLastName("Ama");
        dataHolderRequest.setUsername("testUser5");
        
        response = callCreateUserEndpoint(dataHolderRequest);
        
        assertEquals(200, response.getStatus()); // Should return HTTP Status OK
        assertEquals("00", response.getResponseCode());
        assertEquals(true, response.getUser().isPersisted());
        
        // Clear the DB
        userDao.deleteAll();
        userDao.flush();
        
    }

    /**
     * Test of getUsers method, of class UserApiController.
     */
    @Test
    public void test2_GetUsers() throws Exception {
        // Setup the DB with users
        saveTestUsers();
        
        // Test for when there is data
        UserListResponseDTO response = callGetUsersEndpoint();
        
        assertEquals(200, response.getStatus()); // Should return HTTP Status OK
        assertEquals("00", response.getResponseCode());
        assertEquals(3, response.getUsers().size());
        assertEquals(true, response.getUsers().contains(testUser));

        // Clear the DB
        userDao.deleteAll();
        userDao.flush();

        // Test for when there is no data
        response = callGetUsersEndpoint();

        assertEquals(200, response.getStatus()); // Should return HTTP Status OK
        assertEquals("00", response.getResponseCode());
        assertEquals(0, response.getUsers().size());
    }

    /**
     * Test of updateUser method, of class UserApiController.
     */
    @Test
    public void test3_UpdateUser() throws Exception {
        // Setup the DB with data
        saveTestUsers();
        
        // Get a user to update
        User userToUpdate = userService.findUser("testUser1");
        
        // Test update a user with a new username that is assigned to another user
        UpdateUserRequestDTO dataHolderRequest = new UpdateUserRequestDTO();
        dataHolderRequest.setFirstName("Torti");
        dataHolderRequest.setStatus(UserStatusEnum.DEACTIVATED);
        
        UserResponseDTO response = callUpdateUserEndpoint(userToUpdate.getId(), dataHolderRequest);
        
        assertEquals(200, response.getStatus()); // Should return HTTP Status OK
        assertEquals("00", response.getResponseCode());
        
        // Get the new user from the DB
        User newUser = userService.findUser("testUser1");
        
        assertEquals(UserStatusEnum.DEACTIVATED, newUser.getStatus());
        assertEquals("Torti", newUser.getFirstName());
        
        // Test update a user that does not exist
        dataHolderRequest.setFirstName("Torti");
        dataHolderRequest.setStatus(UserStatusEnum.DEACTIVATED);
        
        response = callUpdateUserEndpoint(999999L, dataHolderRequest);
        
        assertEquals(404, response.getStatus()); // Should return HTTP Status NOT_FOUND
        assertEquals("110", response.getResponseCode());
        
        // Clear the DB
        userDao.deleteAll();
        userDao.flush();
    }

    /**
     * Test of getUser method, of class UserApiController.
     */
    @Test
    public void test4_GetUser() throws Exception {
        // Setup the DB with data
        saveTestUsers();
        
        // Test getting a user that does not exist
        UserResponseDTO response = callGetUserEndpoint(1555L);
        
        assertEquals(404, response.getStatus()); // Should return HTTP Status NOT_FOUND
        assertEquals("110", response.getResponseCode());
        
        // Test a successful retrieval
        response = callGetUserEndpoint(testUser.getId());
        
        assertEquals(200, response.getStatus()); // Should return HTTP Status OK
        assertEquals("00", response.getResponseCode()); // Should return HTTP Status OK
        assertEquals(testUser, response.getUser());
        
        // Clear the DB
        userDao.deleteAll();
        userDao.flush();
    }
    
    /**
     * Save a set of test users
     */
    private void saveTestUsers() throws Exception {
        userService.createUser("Test1", "User1", "testUser1");
        userService.createUser("Test2", "User2", "testUser2");
        testUser = userService.createUser("Test3", "User3", "testUser3");
    }
    
    private UserResponseDTO callCreateUserEndpoint(CreateUserRequestDTO dataHolderRequest) throws Exception{
        HttpEntity<CreateUserRequestDTO> entity = new HttpEntity<>(dataHolderRequest);
        ResponseEntity<UserResponseDTO> response = restTemplate.exchange(
                        createURLWithPort("/api/user"),
                        HttpMethod.POST, entity, UserResponseDTO.class);
        
        return response.getBody();
    }
    
    private UserListResponseDTO callGetUsersEndpoint() throws Exception{
        ResponseEntity<UserListResponseDTO> response = restTemplate.exchange(
                        createURLWithPort("/api/user"),
                        HttpMethod.GET, null, UserListResponseDTO.class);
        
        return response.getBody();
    }
    
    private UserResponseDTO callUpdateUserEndpoint(Long userId, UpdateUserRequestDTO dataHolderRequest) throws Exception{
        HttpEntity<UpdateUserRequestDTO> entity = new HttpEntity<>(dataHolderRequest);
        ResponseEntity<UserResponseDTO> response = restTemplate.exchange(
                        createURLWithPort("/api/user/" + userId),
                        HttpMethod.PUT, entity, UserResponseDTO.class);
        
        return response.getBody();
    }

    private UserResponseDTO callGetUserEndpoint(Long userId) throws Exception{
        ResponseEntity<UserResponseDTO> response = restTemplate.exchange(
                createURLWithPort("/api/user/" + userId),
                HttpMethod.GET, null, UserResponseDTO.class);

        return response.getBody();
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }
}
